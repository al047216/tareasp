<%-- 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <script>
        window.onload = function() {
            // Variables
            let baseDeDatos = [
                {
                    id: 1,
                    nombre: 'Carne 50kg ',
                    precio: 90,
                    imagen: 'https://images.unsplash.com/photo-1583541277229-e36b5ecdd3c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80'
                },
                {
                    id: 2,
                    nombre: 'Frutas 20kg',
                    precio: 50,
                    imagen: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/frutas-1552246920.jpg'
                },
                {
                    id: 3,
                    nombre: 'Pasta 30kg',
                    precio: 25,
                    imagen: 'https://dam.cocinafacil.com.mx/wp-content/uploads/2019/10/tipos-de-pasta-cover.jpg'
                },
                {
                    id: 4,
                    nombre: 'Aves 40kg',
                    precio: 60,
                    imagen: 'https://image.freepik.com/foto-gratis/diferentes-tipos-carne-pavo-pollo-filetes-aves-corral-cocinar-vista-superior-sobre-tabla-madera-aislada-endecha-plana-concepto-cocina_95685-5.jpg'
                },
                {
                    id: 5,
                    nombre: 'Lácteos 50kg',
                    precio: 100,
                    imagen: 'https://www.eluniversal.com.mx/sites/default/files/styles/f03-651x400/public/2017/07/10/498302698_37618972.jpg?itok=NY6s-Iqo'
                },
                {
                    id: 6,
                    nombre: 'Licores 100kg',
                    precio: 100,
                    imagen: 'https://cocteleriacreativa.com/sites/default/files/styles/cover/public/imagenes/articulos/licores-indispensables_tia-maria_cointreau_nojae_jagermeister_chambord_cocteleria-creativa_900x675_0_0.jpg?itok=PXq9JMtZ'
                },{
                    id: 7,
                    nombre: 'Limpieza 200kg',
                    precio: 80,
                    imagen: 'https://oxxo-web.s3.amazonaws.com/posts/ThJnuxUHGIcxQwd8XeUBAR8HkAS5Ltkq4dPzfQ2l.png'
                },{
                    id: 8,
                    nombre: 'Panadería 200kg',
                    precio: 60,
                    imagen: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFRUXGRobGBgYGRofHxsbHRodGh0YHRkYHiggHh8mHR8dIjEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGzIlICUvLS0rLy0tLS8rLS8tMC0rLy0vLS0tLS0vLy0tLS0tLS0vLS0tLS0vLS0tLS81LS0tLf/AABEIAJcBTQMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAEBQMGAAIHAQj/xABFEAABAgQEAwUGBAQFAwIHAAABAhEAAwQhBRIxQVFhcQYTIoGRMkKhscHwFFLR4QcjYvEzcoKSohUWskNUNFNjo8LS4//EABkBAAMBAQEAAAAAAAAAAAAAAAECAwQABf/EAC8RAAICAQMBBgYDAAMBAAAAAAECABEDEiExQQQTIlFh8DKBkaGx0XHB4RRC8SP/2gAMAwEAAhEDEQA/ABpv8NKABz3qRx7wfVJgCd/DrD0pKiuqCRu8vfQAZHfkYteI1AXMy7IDkf1Hbn4fmRvEuNIeRJBLOpRL7kJs/CPJzdsdW0r0nr4exIygt1lMw7sFRl3ExT6Zl3HmgJHwiX/sWRSrRMQVrcWMzKWvsyRe2sXTDJAIaCK2kzSSN0G3Q/fxjCe1ZiDbczd/x8KsKUbRfQ4PJmhKpiApjYkB0k2cHUdNI2pUd3OVLeyT0v8AfyifA7giPcSlNOCgCymPnErsCV4c+sMOHoVn8Kc7BlMHKQScr6sCSW4qMbYPLCnB216biDKY3QdNj5wLTeCepOzwa8QkrsMvz/cXLkCTUKTtqOkO66pSEAqJGmUhJUX5hN2eF/aSVdEwbWPl9iEPaeoUZISCQTvwexh8QAajxA1ugPWE4wt2zTUlLh0tdxcB1LJA42Gg5xFTTTLBWH8XssGc7sNTbUjpvFSp6NYulStLhzY8OGwhnTJmEo7xZOSwCmDAly2UCNTEE3GxoVXSeOsdUWEzJpJLqUS5K3DEOdAW2Iy8BpGtZhE6WrPmCSAWULJKWGrkp5MHtqzOJMC7TolTFJmulKlkpWA4YCzgAq0A0DEO7XibtRiHf5JdPMC0rLrKFJIJe19t7O1haHCKE1Md5Iu5yaANouwuUuctS1ZCCnxFOjAMC7m+/pq8MUBKlCWFZUaZib+egdhva0ZSUQCMgLISWKmLKO4Gj7+toUVcxdNMBmozyiRY6h2cpfWwFiOGm0dBY6jNHeADSvIjLH6GnmyTTzJbpOrG6VXAmJKg6S51GxYhQ8J5tP8A4V1pUruMkxALAlaUk76Et5gtHSMQmo7yWEFGZTkl+FgFJ1SGSeuaHGCTDLkryl0uAg66n6P/AMY14sxxsVPE8/tPZ1dA45Mof8Ov4dzqaq7+rSgCWl5YCgrxn3rflD67kHaL32w7JycSp0ha8ikKeWtIGZIIZlBQdlatb2RfaJcRC1JyS3OxIYn0L/EQy7MUikyss3xMSxLve7X5vET2h3yWOYjdnTHi5/c4njf8H6yUkKkrRP0dI8BH+8s3UjpA2C/w9Wkldd/LSk/4aVJUpW7lSCQE+b9I79jE1eUJR4bgOQ7DnueHnFBxmjXLUFqAylWUlDs+oICjYEeQItqBFl7W16Cd/OJj7Kh8X2i+qT3SUmmSmVJTbKLJzE6nUlTAOojYXNoslBjctdMGYK0XwfR9XL/taEM1ACWUHSXAOoAYG4e1m4hjwhXLp1ypgytlW4Cn8PqedtfehgdUoy6aFSwIpQogrOYuG8ObXlq19uA01jSbSzaVRKSe7UPEwJa9nBsOL9RvEMnGO6ZJSJU0uU2JBsLOwvfW4uN7gNPaRUzwLsp2WFMUkG3TRtujwpWOrmWqplIUlJEzxZQSQSkg8jrf0PW8QSMXmSSE1GdUtrTUJsGv4wnTyuODeKF1NKKA2UlFmIuUg7HV0vu1vUh9MocqBnyqSQ5CmFtdb+REJxGsVUZ0lQJjFMwkbMz/AALR7iOA0tU/fSJK1D/1ShBU/VSbjkfKKv8Ajfw8tKpKkCU/sKLAb+FT6k8vI6xD+NnzkuudMCC+UWSSDo6kNmH67tBGTTEfDfPEGx/+G8oAKTTyZif/AKUuYk8yUpnA/PTaFFN2OpspemkKPBRqQwe7ZZ2vnzYxZ6UqlZhLVlKhxLPsSH+OrPA8zF1rKgJiCzAkm5t7rlIV0d7C2kcuZya/uDukrgfSooR2GoylzSIJ4pqZqB6TEn5nrEkvsNSAOrDktufxa/m6Q0N5eIZU5nc6Es3Szt5PteCFz0LBVlPqfRv0teKd63nJ9yt3pEr8nsjRkkHCXa/hq1u3Qr1jWZ2QpAWVhBTex/FLNuhmDbnDGknKBBUCnXKcyso4hszaXvrrBpUlJYTZhfZKwRpvYk25x3et5xjgS9lErKOxtEPboZg5CePmZn3wiCq7DUBv3NZJP5RMlEf8nPk8XqXOl5HIEwAs+pvd2K9eYEBzpktY8IK7HY77AnTQWa+kEZG6GIcSHlZQZv8ADOUoZpdTNSCzBUjNrxXLW3whFXfw9rEHwBE0H8qig/7ZwQfQGOiVVQqWoygymOyrdOD/AKRNSVSiQ0kEj+qx10DX+doYdoYczj2NSLE5Z/2JiH/tVeqPnmj0dgsR/wDbK/3S/wD9o7mjFMt1AhixYH5g/d9IIw2bIXmIXOLWuFKG+hBfkX4RUZzMx7PW8qlFMdcxWrqJ+J/YQ+xSSTSyz+VXzD/IGKzQqbzI/WL5SS+9p1I4j4jT9OhjxGFuZ7LNpRfSolwac41h6ge1/Uk/C8VWQci2OxaLRRTAoAxOo2Q2Li3DJeWYpPOC8YpnQFD3T9/WPZlPkm9YOqZQMs34fGzfGCuM8Sb5twwg9CXQOMa1cv8Ang8f7x7Qhg0ZiQcpYiwf0/ZoYISIpyAPc2xOXmlqDcD9IqONJJQhtXA9fD8jFhrMVCQ2pb7EVrtEspll7cN4oim7nLkoEQKjU8wJuHZzvpz3c7xohCzMZTuDo3la0ZQTUKCVhmLMo7HViAxbYtDOlnTFKXmMtKx4SoJIKubuSzDaHZTekTUH8Oo8QOdhYUHBfZQ4Nvffnwj2jkLkoUqWQ6gQcw9k3Dgk3bWw+kEVCFAMSdWcaNew3HrodIYIQqY2UOkeEewbbjiGCTfgL7uQG4gLLQMmwnFkqluUqdOVOVLMnbUgXIFmsSb6GPcQqAJUyZUBIlnSWkOpNlZBmsCbgOd4STaYoPeSnSpLuNHdnSRum2jb66NpT06qpedSe7lpLhALh2Dlzv8AQCH74VvJ/wDH8Vj6wSj7OLWcypipKDcFBLtaz3bzfc2iy4DRBPjyJCEGx3UR7xUbnqeUDYnX2ZIsA1jYA220vD6VK/lJSnQN8B+vzjPrNWZXIKFecNwusE8qDZSLJzXJTxh9JlML+UKcGw0AiYS54N8dYbziWsLffGLYRtqInldoYa9K8RViiV6gPyD6bgDeKZiQXNBSUqAZQLhuN+vLpF0raYm4U3Ih26aQgx8KKLvYfIxmc01zb2YgjTKPhVYoFKUrcHY2L6WJtrzaJqqTkUFIZCy5DNlWb2KRYG5fTXaEy0KmTlBx4SSG4jV2833vu0WXDpmZJlTwl+JL3awtz+VtbekVB3EgMhBptxFNRjZXIEmalMplh/D4Ckg+yoktpow3vE82gRNOUqALAJULh9B1HLSIaqgcuUsxISrcA2voGt5wEZEySAqWk5Rp4lNqbMHL32a2xa3d50PMbuaFrxGVDiFRQLTLmJZIOYgeyeaFgWfgba2cmH+JYjLmSlT5afCw8DtcWYAt08tSwiozO00uYgSpksjqz8XBGnPY/CC8Lpwq6ZilIDbb7bsctzfS0c+w3irRNwiVQZlidPOZYvLlksBs7nyD8fQtKkJUlpisqTrlUzteygRezM1xEqkjXQb89teHIaRT+0GLFf8ALQFFjmSfDazO/tAi4IP5vKJLZM5j1jbFcaCVGWlWo4aa3fibf3iPCaCXMIKfCt3LEgK8y7B2PXrFZppBcKUHIu7n4mLH2TnEzFH3QQPIXV/+PpBekUkR8e5qXem7LyAsKnKVOKQ+VZ8IJu2UAAsG1tfS0Mp2HU5AHcyxqAyQC3AEXEKaPE3BJNy5PU3g2XXpbWPPOVj1l2wHrK/imAkKKpMyayQ+TMpQDaXJdr89tdAtpqSaHUuaDYtpe4bxCLrhiw85QP5B8zCDHpiSnO7EqY8G0BP3vyi6ZTsDOAo1UVIl1IJOYGx2BHkLejGIwioIN1L1vkSW6OGg+SEqALlWUaeEjltw87xKot73i1AH384sHPnCyjyiWRRT83sEgke6ePJt4Kzz0oITLAJLggFRtoQ9vpflBxUrUl7ubs/Uhz5l+kDTJ6nBDJtcpKtucNqi6b6TyTVz9ZoQ45i52LDTj92Y4bPUE+IJcl7KP0AhN3airxW4m/p98Y3E2amyNB96bR2ujFbECKkVak0tR+HmnXxyl7LQT8wbEdOMXLs9VBm2jMc7PIrqcS2aYh1SVbpL+KUTqNPUB3vFSoKqopvBOlTFNYLQkqfqlNwfJvlCNi1eNZAZvDoaXHH8MciYi76gfP74R7hqCkDM6RuT+/0ivrxqfNTkkyppChrMHdhJ29oBXoDpEGGYZPmZjUzApTkJMt/ZI0ci510EBsaqNRgUu3hEvGLTUjKsaaa/d4GmVKsrd2piGcsNd2N+O20aYRToQQFXUGYkk+VzAK5jzCpd9gCRbkH2053iJyWbEqmEC1PSNKaUtQZmHMwvWCVtqGIzZ+jBLBj1Ou0bTajwFI0UQGTqbgceGttoGTNG55NxfizPfYbc4Nm6M4Ywd5uMOSXIDqO5urhbMGB8oU4vgXepUQpRIHsrLB+ZSAW5CG9PNcqIJtqqzC+unw58LwZLxFOqU3Or9OAf0+F4Adr8JjNjA5E47LqJ9MSmfJZlEMkpYB9tHEMcN7UyZk5SEqKbFswsq2g3HTgY6aaSXOJeWAryLtoXIcffGAKns/IqAUKkJW25ABTzCtj0jQM6sd138/8AImh1FBtvI/uIUVKZjEKBZztq3qzXhth5SGHhzDioZg19B6tfo8U3HqFeHLzErMgn2wMxlufZmCzjgoNw1uXmE4jJnF1KClFvE920BdJJtw2FnEEkjxjeOCrDTHmJy1ZFJTrmGoctexURY8QLatziVLUmXwypBI1L9DxL+t94Nwqeg95kygS2KgNb7kMw35fKDp9OltuYVoU8CTw1+3iD27WZVMmgaZQaquT3gC0nbVTMR9G9fOLp/wBWySBkSFLUxDjTb94T9puzcuajPLJQpnTuOhG3k3nAnY9ClyEiZ7UsqSf9KiB6ho7bRtA7a2GobS+UeNoSkZmCmfKASfQX4w4FSFJC7hPAgg+T6whosMGZMxvGkXFiPsNbzh1Pp1KbhFcZcLtvPOyjGW8oHOxKWx2Ox19dIrWP4gFpcF0i2tg+u2rdNYb4pg/vFlJAfI5D/P0aKhjc3KnTKjg7i+3XZmEZn13TTf2ZMfKznhxju6qcDcZgQ9rFiN7WPwh3USlT050e1ul9RZifL5CJJ/8ADZdSvvhMTKznNoVa7EOPnD/Dv4eVMggpnomDmlSPkVPfppHo97jKijMVMrnUOsX0uNLyiTMQTZsxszja19hfcaxFUJmyiSC6DwTppqkaWJD6QfiVDUSSVTQGIyhQuPNr+Z5QIJ6QrUjpuOIeEsMJpQleIP8A9PkzkuxzPfYN8WPQDWGtBSplISm/Ek6cweLfe8R0ssrUpY8QQLMA5IZy4S9rhuR6xBjOKpRKE9yLAJTbKXc5zv6NdtYQAkw5XB2g+NVkxahLkAqmMfZJbK2uzte3o5ET4N2XQlIU5Uo6kk6+UHdgJSTK75VpilZid+I+BHqYsElSUzVJ2LEee3qIzZsp+FdqlcSKNyL2iiZgKC4I+JHyhNUYcqlzrF5Oj7pKgR4g3s6eL1bU36bl1hfPkpUmck+ypKRfqYiHINE2JTbkCUyXX+cGy8S029IJwbsXJl5nUtSXdCSogIHANc+Zg6p7LyiCAFJPEKP1cQGCXtLrlseIUYMnFMsuxuVcW/NAOLTnloSLqWoJADuWclgNTb5wq7Q4LVSU5pJ75KblOiwNbJFlb6MeUP8AsVTKy55hzK+CXuUp+vH5VACDXdyDNqOkCjzPaLDZyU3Yef6WjcSZgUXDvw4/A+UWtZBDkCFM6XnJy+yNVHSJ62uxHDAjeKhJUdfThEC5XH4vDSo7QSqaXmqGUkFszEkPo+7bacBDjCZoWkKSgISoBYcMQCHYjieejl4rvQMicpBIqVFVOo6ajR38o9l0xFla+f0B+MXBchw7s/C3ygCbRX4wpzVGG887K4sZqGIImS/DOTuw8Im9UkZVDiH0cxZqylTMAmb2zEWfgqOf1NYtEwYjJQUF8lQg+7MDJzEflUwSo7EJOjmLthFcDKlqT7EweFJ2PvSi/DZ72PAk7VOk30nnZV1bj376RLi8xlIQkspXwGgvzv6c4Y0uHEJAA14E/YvCCmqpdRXKYkolkp8XFDuGOjEs3IwacYVNJU5CSTkQHskCxIAuo6seHKM+U63uaUQqgA/kySbVMvIgAMSMzXO1vu/nHikMWIBd34nQcfvS8bKLs3iD35X4nVvpHuViUlR6b/2d2+ERU77iWIobQCpm5SDZnAbz9OX7QBVVBKsoO+mpPJtTBvaGiUuUru3Kx4kF9SNvPldy/VRgayiWlSwRMIBWFC4J91tm08oJxnmOmQcCOpVPMCcqQG3ci54mxbgNdzYx4AUOVBg3tWsHJ4W1+9T7LxNLsxD8d/Q2uN+I4hyZ9eCCxFtVHQffl8nYA1UTrc0pcQylwl/Tff72HIwfIWrUOm7nb76/rFamYgUG5AGt0qS/Nj5XBOwtaDqPFs/hQCo8Be3ltCfCI5TVHNWhE1BROQlaVBiGJDbj7Ec0n9h/wlQmbTTCqU5KpSixy8joprHY23joVQqYE2lLJ5AN6PmPkIQrqO9SpB9rMwfaxtpxcw6ZciX5HmT7nG5vygUnHpKSVZUBSn9kEFy1yq12AHlE47T5RlStydL7n6ffXMM/h5TPmmzJyySSwVlSOgAf1O20a41/DVJAVSVCkTEv4ZviSeRUBmTrrfpGilY8yJfT0hE/G0sA4+xC3sFi6fxFTKOuYKD8Db1cRz/EamfSTFSp6CiYNtiNlAixB/Xd417I18xNV3xByqcKPI7/AA+cWXspCkyWTtSsQo859GIxAswJAt5w0lVqTqD1EU6gxALRlYvxa37Q0ppmYX1G/CMnfsrbRsnZwRGuISytPgU3+YW6gCxMUnEJKVLCFMoBQBbci36bbRcZNUVPfKOJMUvEqcIVrofkYXOwNEdZTsYKkqZaMNQlICARyB+UH5hflry6wvwqrRlBJ+MDY7i6sispygB+ZG7QqsAvrJsjM/8AcAxKWhRVMWco4KPusXDcT9Y55gFBVz0kCSsZd5icr8G7xncXtF57OViKpp48QcoBexZnV1uQ/IxZ1VAMxfLwjoP3jsTlAQedv7lclkjTwBKRJopsmSE5VBnzM12Dj2X1L+kc07Ud/UklKQEJ90cRr8fpHfJ3KFM+hSsklKdPaP25i2LtWhvEJLJhORTRlO7BVmaSEJdRAAI0IIABfhxi6S8MUSFm1mA3P6CMwXD5cvMpCUpKj4iBw+384bJm3d+Q5AaCM+Uq2QkcSql1UDrApqCnVL+bfSFWYqmEEFKQBtqS+nT6iLDNvCesWAW/vEm2lsfiMlkMDG82o91IzHb9IgpqlKnFsw1Hk4+EE0iSHISQo6E8OQ1/tAWxzCxEyZh6WzTFFSvyoLAebPCdM0SV5WZCnIOrK3Btvr6w7UV6AOfu8LMXwmYuWRLIE10lyWAvfRzo4hj4jXSBW0jcyRbqYB2OseTpSiAkWQNhqepGkE4XTKRlz+JbF7MNtOnG0HTvLyMLRA5h7zcbSq12HTQgqQlCjZk6HW/tW05wdR1pYggpJYMQxY62O1m84dIlFWnMgbxrUUaSliXu4ZrdDw6Q1Gp3egmjIpVQGvAlbiqEMOMTCiHNo3OBSyxUlL83f5xy2YCcYMW4ahE5c2bKyrE5OSpple8QMqlJ/qaxTuAGOxGwoKpZiqScpRkrYypm4D+Bd/fSzHjlLi90ddIm00/MhTLYEKD5ZyNpgA94aKGobcM9nNSmvkBEwd3PQMyFPZTs4zaZVWvxYtZo32R79/7Mtdenv8faRdpsPVLV+MAGZAy1IBspLeGcniwN+TflIKSlrAUpI56er8/2iz9n8VcGTPtMlg2UPbQxJQ3EagcyN7VLtZ2XVRkzqYvTnxZdciem6A+t8oIe1wpQE2IyZNPgaW3BK4FYzEepNuLu37vxiDGUqQu46HYj5bxVMKxnRze+p0c89v2i302IBaMii6fVuY4RnfY1NKqR4hF0rFEDw5mfUG7/AH98YLTKTPASxUWsUhTjnbS/7vdtKqklSvFlBGxIuW1c2vyhxRoWj2AlL7XP7wUck78QZCumwKMqdXhFZJJeSspGiklJfnlSoqHRi3xhbIxlLjMd3/1DTzB48BwjqAq1EBK8gJ0yhRJbhcsOcJsf7J0tX7SCia3imJdC30CyPZXwdT6ajbSVQnbiZxmYfEJQZ8qdU+BSlJExVtCEoSoHOBqCwbmVCOh4Bh6JaAlCcqQBzL8S+p01jnmHYTPpJy5U1eYkjJNGipQ0yhT5S5OYaggahjHRZGDolAKeYqYwJK1qIFtMosfMHWFYUavYQs4Kg+cOnzCA5FucUqqpCismzc+YKQjKk+7dWYPwNm4XEXiSkm5V0zfID72gfEK9KAACFE8dNeH7QhNxcezUBcVSMRSkblr+EP8AfrEqMZlrDAlKjsoEEeSoExUBKTM/wgi6wGZt1C7BtTrZ9GiDCqX8QplDNKAzd4kEJJBbK+j8WOx0haIFSxCVqMW9p+z34wyx3ZWUksoDQHUE6agaxqrskZEu6A27bdY6AlGVgLNBAIWLi8UxZ2C6fKZMgUtqlN7PzUiWAdrHqPt4fU0xJcZh+m/CFOL4X3KjNQGllu9SPd4LHIb8AX0BhlSS5QSCACTcffOJNubmgEFYSKhrDnqYp3bOuEuXMzBrHKRdySwLNsTfpFknLuC7GMm4MJoT3xZi6QGfhcmw6RJN2APEY+BSw5lJ7M1S1ABB7z/K5PweGXajD6mdI7tMqalzdQSXCdx56ReqOUhACUHIgbML/fGI51VMdQTcAC7jd9Byt6xY6VbXUj3jsNMqvZqjTT08mUk/4ab9SorV8SR5QdJqCCX1JMA9pJc7J3slu8BDguy3swbQuzG+9tGZTvdWuSQsgFSQrcgOxNi3LWIsC3j8zLoQtJXSESppJ48AI3RRrWXKWTmBL7gC3xJieTiMtKXTp00iJVWqbfNkQDclw/mdB8YouMVubky7XsKmKoglebvNbZQDfgfLpwgpFCLFRyg8TfyAgM16UDwqCzbpz6xCKxUwu5Sl7qPyBNh58IFL5QeM9ajM92GCQTw1+2jFyQm8xuSQ3xJgY1KUhk6/mO/3xgfxzXV4gnUmznhrp5w23QX+IoB5Jr8w3MlyEIGZttfOJkyGus+Q+pgeXVIljKkOdz96wBPxEq0udrx1D+TOon0H3htVVpD6Dp6RFh683jUWSNOJ6QNKoF5gVgcblvRGp82Fh1hXidcmWyZi2WQ+QBy2hOVJsLG77G1oGg8xg6/CJY6mtQbWA26/UxtJkBTuSAA+tz9YW0plpTnWVaWTl1cAuNSkbXjJ+IhQAyJ4jiDxfjBoddzFvouwjOZXpAZNhwv8RrApWpagE6bk/vvCwUyyXKk8dS46kaD/AFNpEs/ECE5JJQBusG/k/wA+e0NpP/aDUB8H1jtc1EtOvi48Po8LRVvcfP8AWFspDgGZNSBqwJUT1a3xiYYwkOEi3FQcn025QCB1+04bcfeJ5GWTMNBVqJQp1Us7Vhsx/MnQj3hxvA4QmTOEmqQe6ckTEmySoMFpVqAbsdNUkWIi0Yv2YE+n7gqdSC8iYXdJGiSdT11PW5SU6vxMkU9SO7nICghRZlNZUpRPEj4Ai0XsVfv376zlJPv7+/6jJGGBJlmaoEJUO4qkauNEzE2vqMtwbsQSws0ygStIzgNqnKbJVoW/pN7HYkWtHLaDGF0iTJnIE2lX4QJnuqBvJmPz9km4IAfQw2wrtslFT+HJV3BbIpRdnHsKVux0Ub2vcZiarp799IrK56ysfxDwpNEtapAKSkpMyV7mVRZK5R9pIJtkuB4mYJaFeB9pdL2I3d/0P94v3b2QTNkrqECZT+IJmpeySASVgWBSRz9okWzRWpnZaVKOfKJko+0nZQ4pI0UNbcN7RTSjrTDfpOTI6EFTt19+6jrBcXM1aZR8SVX6ZQ7/AA+MWCvqQkoU5ACgDwin4VgKaap76VMK5MyUoSwS7F0k3N/Zca2fnZ0M01QdwkfZPD1/aMzqiCuZpRi7auI4xuepKxMl6EWvYggEjyIf0jehxBcyYkszaB3udS/w+7BHEGSZaJYmJHL9bx5RViC+VBQ+jE/Vw/6QhY16Rgnh3HEI7T0iZyEqy/4M1BcaMVDMG2DPbiEw+xCuSklAIfi/BnH3xhRT0lUZKwmXlKtM/h53FzfpAuI4bOYzFZV7kIzOObFI+DmKElVHrzIaUL1fEaTMWSGe40IIDfvAtXLlFPhNjcXe51T+0V0VJJuQofPrzhvImoRJdbOXIYceD8YizWZoGILVcwGimJXOQkqKgLKBUS4Zi4NtLRd6SdJRKISEy0ywBlSGAGwAHyipSq4TFWRc5rsxdjf1hphFE6WmLCiTmI2dm038+MBchBqL2jGGFttUNp8URNUUhJB52LdP3hnT0y8r6jbjA68MlKACUIlTAXStIAvwUBqD9YIwqruUnXccCLKH19Y0Y8Y1DUefKYcj2vgHHSLsSK8rgffSK5VVCkLyiWq3sskkN+Ww206NFtxM3UOIcff3rBPeuhJtdIPwiPdW5s8SyZiiggTnOKY6QZYykErFiCCX663i0SKtTBwXbgfWCquSZikykpSoqLl9ABdzYtpwiaowlgQVvxuUt/qBEDumI1ASj51NAxcqcobM/T0e5jROICWFrmKDlkpSPU/fKFNP38+YRImHuwSCtSWdrMAbqHM3OujPLiOC1KQmYAmcUq9g+HMOIKlZXGvC3lAGJyY+pOGMPpyTexyt4d82XMfNilvOA8bxL+XlzZTq40fp0hLhsycidVKmDJmmjKl3sJaU676fCB8UVmcvrAK6WoR0AItoNTdp0hTFQfmCPo0M5eNpmb57NZT/AKvFJrJSAvhAH/b8+etpEorLs4Fh1UbD1jSMYPJqTbJyROmS8RAF0pPUD6xvNxEqLlJY7AFn4gM0VTCOxdbKWknuyQX9pRb4bRbh2TmTktNnqAOol+EHqXJ+MTKIprVfyg13vVfOZTYmhr+I8/qBGs7E1EMS48h8oXp/hfISvMJkx+Rb0OsHnsAgjwzqgHj3q/kTAIW6B+3+zgw59/ibyahDEqUrkBb4n9IKk4mUpaWkIs3hDm19YUDsAAPFV1J4/wAwxEvsO/sVs9PVRMN4eLikXvHaa06iWVHZRBJ1exN9eESyp61EqMpST+Yp0A0ZR08uMVed2NUNa2oV/rUPkYj/AO0EH25s6YOC1qPweB/8h1h0MY/rcdkSRlWuWG2KhClXbykll0pCjyzK+TiPZHZWmTpKT5iNVYBJ2ljyEEZUHnO7i4DU/wAQ5RSQqUZt3bIm3+5oBm9vJarGkUOaSlJ+Fj5gw3VgSNpYHpG8rsklZAIIJ2A+pGkMM+MbVCeznmVhfbfLpKVl/qUH+AaNZfboH2aeYrjlL/IRe5fYyllm8pMxT+0q4HQKt5tDqmw5KU3yoG3PoI458fRPvUTumqy32uWT8OSHQp/6Tf0OotCLFsHSpZKwQiaQFndEywSvkfdOvubPDLDpq0i4foX+Dw1TMSsEEai4P1h0UH0P2mUsyHzlBnYNLnLVTzwFTSnVOk6XoFN+YaHh0ymEczsqmWVSC6iASgn2ly3YpP8AWkt8OJhX26w2voq1EyQ65AvKUX8N/FLJ1fmdQQbkFrRMxJdVKlV9O/fSH7+T+ZDMotupI9Re7JhjjYAb8+6llzC7Hv1gHZrH5khX4WcBNkq9l766D5sdiG2eGM6fIkKzSQpVIts6CD/LPFJO3L3eka47hfeI/ESUhSVAqAHtB7rSOdswHIiG2A1KlS0d4kBYcag2FgrMLOQ1xxhQ3T3cdlX4hzFpwIhZCVpEonMDlUSCdWDCxGoJA0I3g6dgZSnwKWr/AEhL+eYny6Q3lTQc4Ico4ffrAsrEQp80zKXPLTRh73QRInUd6uOmpfhlUnYqlFkomEJcWQQxGo8W8Xzs3hCJY75Sf5qg5/oe5SkbHieL7QlxRZVSTVmXmKAbBAU739kD1F4PwqvXMlpUFa7/ALxXFkHJETtAYiga6GM6msaYEuXe44QKuqEqZlXZDeFnYX4C+npESZeRSiou/T5s5cwDU1aLIU6golj7yS/HcOXY84Uk2WqKiA7CR4hh8lZUZBSSGUoAhiCdH90uOI1Gkc/xLGlIUQQA1mJ0azMQwZtOUdEkTJMtUxOZZUzBgABmFi5Bc9Y5t24wFc6sphLLfiHEwt7OW5mN/ke3FHqECs++0sXbGhIs8Sz9jJKqiT3qjlCyWsTYFvo/nFgm4JOlsuWsK5aHoxsYkw9UqQhKE5UIQAlI5AQeMTSse0CNmiHhYlj8pzPkFfeB0WMBTpUMqxqPrBFPNSZneFSgtOwZlWYEuHdrWI2eEnaVYI71JAWjU28Sd36D4PwhajFQAFk2Iv8AfEGKLsQYpQMDUseJYjmUG3B+kQKxgBID6Boq9VjRmLaUhR/KAPv7EAijnTJwl3Jvn4S3DuovdtGA95L6EQ5QkkmFVAAEvOF47KlvNXMAJJSlO6mAJYanYdesbzq2dU5UiUtMtRdTuFKCbs23iHmB5HzBOzglkEAFgxKm01JBy3N+lyOcWBCSlP8ALN297MfUOG22iqoao8TNkyIDa7nzkZkKQj+WpKW9kNbRmtt9REQpS2Za3Ufe1Ueji1uAiHHaOZNl5TMSgpIKFaAkaBVjZ9hyim/xE7WpShctJUH8OZKgCdfZA0v0PpFGI4EljUt/ZnmNCbVzZn4cpSJZCQogEKcCxCTtpsRbjA+G9i66d4Vz5SQ/iIlqLDgHWHPWH3ZepkGUlEtXiRaYFEFWc3Kj1e3JhaLbJrEJRYiFIReauVd34W64iHDex1PTF0pK5jMZkxlK0YsAAlL8gLGHX4cIDJAgSpxUDQwBMxl7PGVsi3GXE9bxoEDfWJCAkXty+n9or03Fmu8Qy66ZN9kEj8x09Tr0ETDVwJQ4ieTG0/EQnY6+nOPF4mBaIKbD1Zv5igH2BD+b6ed4KXh6EgKCXDb3fzJsPsQPFG8HEWTasqNr9HjdMhdrHztBlOvOoiyEJ3+/nG86t91FwAfhxMLW1mU3ugIsNOSWguXh4AzLe50diRxbYROEIlJSuYtlu+zk6sHfhCzGMRK2KSEoL3f2ms3hvbmwhhi852ssaXjz/UJloBUEhIfgw+cT1tJLA8KilQuSlvTTSKXVDg7cbP6D6fQxrRV06WygolL6KuPIfv6w4wbcx2BvaW2nlLyCZMKEuWDkJe9jezmDAEywQFPm1Jy2H5QRqIrGK1i6oJsGA02cbsdDzeBaedMQwKFKy3YKA+f2Y44yNlHziEWLY/KWuYoWUBmGw0cvxO3z5xGvFEjU33b9NoqddXVKgWl5QTd1IHLdUKFqqhohZ6Kl/VYhhhaJqXrLhhuMm14tFFiWYXMcXpMaVJV3dQUpVoFA+FXmdDFrocUte/P9+EUKFDIHSwnRq6SmcgoWHSR9kfOKBSy1UFUFaJdljZjoscvo/CC5Xa3IGRLKzzLDlBdLU1FbLEwCnSC4KJyXIYkOPAoMddYZXuwff1nDERv09+VwmVJ7mZNpklkTEmdTnYN7SB/lU1uChAMydkuk624M7AfDQwTUTpiMomVFGnJ7LofLZnAKA3k0eS6kkMKykHSUDb/K4+sK3iO3Py/cZBp53+v6mU03LexJ2YHN15X8x1jYy5JJJExJN8oUCnyGvxgWrrlS7nE5aBx/D6f/AHABEdPiKVAn/qxUOKZQA5/+oYTutufxLd4L/wDf1Dq7EEp/ltllgObe0TxO/wDYRVuzfaUJKpJFkqIT/lc5Tflb/TFh79KvZxQF9lM3oxiq4v2MqlzBNpplIpYf2JmTOOGUpyg83A0iuNOl7/yJN3AHG3z+suxxEBiCDb7eFFTV97OCiGAvy4fpEGGUNahA72nVpcJWiYfSUpRfyiITshLqHLl8ft4GUtVGHFpBsQ2unJCspchZSO8swZ/DxBvwvxjXE6hCZss7tMCSSBrl3269YBmsQQ58XA/Fz5+sVTHMdCq+nlpOZKFZVnYlZSG8m+MDFj1Ax8mQLU6jheBSFkTJ4KyQ6UE6dQIMrMGoynwSkyVfnSkf8muRAuFTbO94NnTBlvC94dNVInH4rszm3amsmIm/hEeOYsFuBQQRnLOw1HF7awVSYF3aAiY65uXMXulCfeWrYbsknhru3qwE1Pf5QciAE23UolhzOVPqIsWEYazzFXmLDqVx1KU/5QDYPDp4gABHdiniMS1sr8LSrXL9tvCrfNoLmzv5aMY1/hzhk2VTZp3tqUVXOg2udATpcRF2hnrmzEykozZcy1JBdThIYWDFyo32KBza3U9MnJkWHSGuTqRxt69RFQN6Ezux0WeT+J5g1KhEpIlkkCxUokk7E5jrfoHduEGzJmXh14fH7eMWGuG1676fb6RUcdx9SXIGRGR0qUPEpdwwD3y63HDjDMwUSKIcjbTO13aNMiUU5hn2yuMqRoTw8LON3LbRyqgJqppmrBUBdCXAKjxdVufkA8WLCexc/EiZ86YqVIUoqSBdSySSVeJwlD6C76w5qf4UADNIq5iZgDJ7xKSk8iUBLeh6GApUHc7/AGEuzUNI4/MS4hSqzS0ylZ5igHb2nSknjqAAbl3HRmA7VITaZMCVsMyTZT7+D2n8oD7P9la2VUgTkkq/MLoA4hXPoDpaOoKwdKkjMMxG5At983hciIdifpCMxHHHrOaqx2ZM/wAKRPmdEZR/zI+UPMFwefNAVOHcA+yknMsjiQPCgcyTFlVTiWRbfh8ekZOqyLBQA4bqPFR36RBzjX4ZVe8eBycHkSgVLT3jbrUCB0AAT6wzdKtk8jws1ufOIZtMhaSmYSXZ2LaXyuNBb4QTLppH/wAtJ5qGbydTxHnrCaHncHmS0uFDUaHXzZ7xrUpWtTZ+GmmnD6QwRIk+4hAHFIA+Ijenoxnz3DadenKCMVmhF74DeVnEp5J7tXhloIYDUlmc3t02+MDyqmXKOZU1ywKUAXIc6nhpwNoteL4Smcghep0I1HQ7fLi8cm7R0dTT1CJa1KXKU7TQGACb5VXseGx9QGGJtW8qmZXUCOsQxVc0uSVDZ7A66D7+kArmHiA/C3JreXrECk51WDDbo2122e5jRUguxU3nw+9uUVUACaSPKFCnPvK1uBw2/QQPMtd+LfuHeGEtUqWQtRzOLsz9OI9RAkuV3sxXdS2SbeIO19mFiw8r3igWTLySTUX1Dg6OnVxxAY8+MMJVQCHK1pYaOlSeDgoXmG1jAScNSVELSAQL+M35+Hd7s+/nEie7lBu7S50BuXG3i4eRvrwepEtJpgSsOJyUt+YFO5tdRfb7eBV5fenIJ5LDN/pPzgxE1B8WRCR/SMrs97f2jX8Qgm6Rw9/18IaBpg7yoymUQmp8VJSl9lFPp4Un5wrrMACQMmHW3FPOSi/Fu+T8oO/7fmJH8mXOlnlUIWD0M1alDp8ojl4RULDzVVaDy7k/+AV8DDDaYzv1i2mwlYV4sKnKG+aoQu3MKnsYt1BVTJSAkYZNSkD3cp+CJhis18iokspE2a39ctY+QAgOX2lqwW7+SOSiX+KoQtfIjhT5/mXZPaRQLChqgeaJjeptEs/GZ+UZaCap9cq5IbqFTkmK7S9pqwBylMzmFfQ/rBM3thVgOabMORSfg8KGHH6nFD5fcyap7ZJlryqpJiVDXxJDdXmfG8ey+20jeSR/rB+SjAqO35AHeU6xyKU/QmJZfbOjmHxyEsdXCT8FCBfuo4U9V+8hrcdoZv8AiSCrnqf/ABgAHDNUrnywNUpWhj/vQT6GGycSwdbgy5SeiUA/8GPnBKaTCiLITffxn4lcd8x9BGuujfUxXJpJa3mUq6kKaxTPlkEjbKtRAPQAmK1ieG4n3gIo6icD7SlKQS/LKeG0dJoZdAhLS0SQP6km/E3Jg5E2l27oDcBSh/xAaHQJ139+kk2Rx8Nj5Tj8zC8UUMv4WbJBs5ClHyyjKPN4gpuyEyV4piFJOrqBBfV3Ot947PNxeQn2ZsscXCj9YBqsezMlE9LkgWlKY33LlhziveIuwiVkc2f7lawrEB3YU99FDgRr98xEWMdoRLQVFgBxhrUiozuaKnnuWK5C0JW12cqyKN9g+psYqfajDKacp5mH1negMlyoJbkVjIQ/CI90pb0lhlIHr/Mn7FYgKpaJqnAK1L/2OhKb7EueLnlHRauaEJJJtvs2506HnFE7IYWmnIS+XMWGliWPFtbmLHWzCxVNy5UqLWIV1GcAaagaObx2oKxAiuC4WK+z9UmbVqXlIdDMoXACib6ceG56xbMUplTJWWWvKoEEGzFjobWB48o5TRY+Px6RKUCpSh4QSbEsrYaBzbYx1WTXoKsmdOcapcPsdDyMMpokHrBmHBHT+ppiJKZZCFMoJ1sSwF2fU6xTZWEGvXnmlQkJN0Zh/MIN0uHATsSDe6bbMP4gY1LppCr+JmygkOWYJGh4XGnlG2C1gCEIc2SAObC9+O8SyvpIMrgS1Pmfx/ssqJjAAAMLACw6coMkzi3MQoRMgtE/pGdHInZMcYTZqQHUWfhASMTQQyX9P0ibuituERjD5aC4IfkN+OsXJc7iRUINm5gtaVM5DGKNOrlf9Q7h2SJRnAnh7Lf7n9BHRFU4mWCh5wDN7MIM3vCslWXKGSkBnfdzx9YiuIkk9P7mlM6oK4/UDEwM5UwO/wDeNwLfmfg8GqoSgMCT/mD/ACgGoXlsWSeGj9DE9JB3lQwb4YZRpu51+UFiceP3whKJ50B/v1Ow6QfSpWWuFPwBHm7/ABtDKTwJLInUxhNnWvFV7SYPOqZRRLTdwpKlFg4PE8Q4txi2d0E+0xPy/fnENRWAbxVtjbHiRxsRsglGk9mKlCQDkNrhKv1a2mkC01B/OTLX4SXcnUAEgMDxPwaLjPr9SLtCecUTlFWikgMb35QFyC95sDuQbi6skAEhLnmQNeFzva+28RrmBHtFJsxBSb6e6TxbaBq2tUhSkixcjkxf0t0f4wHWVDhnIu93JUdxwYWcm+20a9uklueZ5OWH9lIDnRxb05/e0M6nDBnCW47Fw5tcvv8ASI+8YNboPv5R7+IcNo3Xyv8ArAEYzdFGoexN0HhAUq5cahm+G0eFNSknLNSkG7KSH9Q31iWlqLu/oQfT5tEM+rS91Xvv9AWikl6RhQTZClOiurByUUKHmBKT84ZspXsYjlO2amKi/ULEZGRN2oyCixDqKpqJaildUJujFMoo9XmK+kOjmcBSswZyT+mkZGRhLlmNzboVVFCQUOHiZNXnlS2YsQlOY83a3DXeBMXwGUSoy1zJZSl7LUQfIm3l6RkZDnZQf5kAbciI04BXs6VSzuHmf/zEarw3FU60kqZ/lmSx/wCZjIyLqB5Sb5GBqFok1Sh/OwoENuqm+Ym3/wBsCDBZKyXoJso/0zkAf7RMKfhHsZDhbid6RPE9naUgkmoQAWPilEP/ALHPrAlNglKmb/8AFTAR7vdH5heU+kZGRNxo4mnES4syyycPIvTz0n+hcvK/+pH6ecAUuLd6/eUmZtSoot08ZI8o8jIQG4a5kNP2kwtRUkKmypiSxy57EdQR6GHuGVClh5NaSOEyUS/VQUP/ABjIyHzqMTUPf0qLjJdbP7/NwudhcxQPeU8tbg+ORMKFAaucwRf1ir1XZaVVS1JkVc5a1OAKlSykFmY92EuA24OkZGRRgF0sOp9+sjjdmDDivfHE5srsrWYfUImK7tRDkKSqxDMWcBQLHVo6DUdqJOVEybIQuaj2VlIJToQQ+9hfbXlGRkNkcuAx54jYsaqSvTmcp7XdoF1c0q9lKSWA4vc87v6mOl4DWgISq92/WMjIPbkCooEPYXLOxPpLEa7QPfhfl5Rt+MKRmzNyZ4yMjzGE9BY6osQUUpfcD1ME5uJ6Dj62jIyKseBMhAsyZNKssXyctT+nxMMFBWx6mMjI0IgHExu5PMAqSrUEQmxMFSS7Oljy+2eMjIzZOZrwmD067A8bD1/t6RYqc92ANVH2jp/YRkZCKaMfOLAH8xNi+IKBLWeEU3EVEubffGPIyDV7mVXYbSVNcSG9YjXUGPIyGIiiJu0XhyTH1OVTPqzgt0f0ELbFybk7l/1sOQ4xkZGvGfCJPqZglsY9DG1rO8ZGQ4EUk1NRMccXHkW++EYT9/2jIyDOuf/Z'
                }

            ]
            let $items = document.querySelector('#items');
            let carrito = [];
            let total = 0;
            let $carrito = document.querySelector('#carrito');
            let $total = document.querySelector('#total');
            // Funciones
            function renderItems () {
                for (let info of baseDeDatos) {
                    // Estructura
                    let miNodo = document.createElement('div');
                    miNodo.classList.add('card', 'col-sm-4');
                    // Body
                    let miNodoCardBody = document.createElement('div');
                    miNodoCardBody.classList.add('card-body');
                    // Titulo
                    let miNodoTitle = document.createElement('h5');
                    miNodoTitle.classList.add('card-title');
                    miNodoTitle.textContent = info['nombre'];
                    // Imagen
                    let miNodoImagen = document.createElement('img');
                    miNodoImagen.classList.add('img-fluid');
                    miNodoImagen.setAttribute('src', info['imagen']);
                    // Precio
                    let miNodoPrecio = document.createElement('p');
                    miNodoPrecio.classList.add('card-text');
                    miNodoPrecio.textContent = '$' + info['precio'];
                    // Boton 
                    let miNodoBoton = document.createElement('button');
                    miNodoBoton.classList.add('btn', 'btn-primary');
                    miNodoBoton.textContent = '+';
                    miNodoBoton.setAttribute('marcador', info['id']);
                    miNodoBoton.addEventListener('click', anyadirCarrito);
                    // Insertamos
                    miNodoCardBody.appendChild(miNodoImagen);
                    miNodoCardBody.appendChild(miNodoTitle);
                    miNodoCardBody.appendChild(miNodoPrecio);
                    miNodoCardBody.appendChild(miNodoBoton);
                    miNodo.appendChild(miNodoCardBody);
                    $items.appendChild(miNodo);
                }
            }

            function anyadirCarrito () {
                // Anyadimos el Nodo a nuestro carrito
                carrito.push(this.getAttribute('marcador'))
                // Calculo el total
                calcularTotal();
                // Renderizamos el carrito 
                renderizarCarrito();
            }

            function renderizarCarrito () {
                // Vaciamos todo el html
                $carrito.textContent = '';
                // Quitamos los duplicados
                let carritoSinDuplicados = [...new Set(carrito)];
                // Generamos los Nodos a partir de carrito
                carritoSinDuplicados.forEach(function (item, indice) {
                    // Obtenemos el item que necesitamos de la variable base de datos
                    let miItem = baseDeDatos.filter(function(itemBaseDatos) {
                        return itemBaseDatos['id'] == item;
                    });
                    // Cuenta el número de veces que se repite el producto
                    let numeroUnidadesItem = carrito.reduce(function (total, itemId) {
                        return itemId === item ? total += 1 : total;
                    }, 0);
                    // Creamos el nodo del item del carrito
                    let miNodo = document.createElement('li');
                    miNodo.classList.add('list-group-item', 'text-right', 'mx-2');
                    miNodo.textContent = `${numeroUnidadesItem} x ${miItem[0]['nombre']} - ${miItem[0]['precio']}$`;
                    // Boton de borrar
                    let miBoton = document.createElement('button');
                    miBoton.classList.add('btn', 'btn-danger', 'mx-5');
                    miBoton.textContent = 'X';
                    miBoton.style.marginLeft = '1rem';
                    miBoton.setAttribute('item', item);
                    miBoton.addEventListener('click', borrarItemCarrito);
                    // Mezclamos nodos
                    miNodo.appendChild(miBoton);
                    $carrito.appendChild(miNodo);
                })
            }

            function borrarItemCarrito () {
                console.log()
                // Obtenemos el producto ID que hay en el boton pulsado
                let id = this.getAttribute('item');
                // Borramos todos los productos
                carrito = carrito.filter(function (carritoId) {
                    return carritoId !== id;
                });
                // volvemos a renderizar
                renderizarCarrito();
                // Calculamos de nuevo el precio
                calcularTotal();
            }

            function calcularTotal () {
                // Limpiamos precio anterior
                total = 0;
                // Recorremos el array del carrito
                for (let item of carrito) {
                    // De cada elemento obtenemos su precio
                    let miItem = baseDeDatos.filter(function(itemBaseDatos) {
                        return itemBaseDatos['id'] == item;
                    });
                    total = total + miItem[0]['precio'];
                }
                // Formateamos el total para que solo tenga dos decimales
                let totalDosDecimales = total.toFixed(2);
                // Renderizamos el precio en el HTML
                $total.textContent = totalDosDecimales;
            }
            // Eventos

            // Inicio
            renderItems();
        } 
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <!-- Elementos generados a partir del JSON -->
            <main id="items" class="col-sm-8 row"></main>
            <!-- Carrito -->
            <aside class="col-sm-4">
                <h2>Carrito</h2>
                <!-- Elementos del carrito -->
                <ul id="carrito" class="list-group"></ul>
                <hr>
                <!-- Precio total -->
                <p class="text-right">Total: <span id="total"></span> pesos</p>
            </aside>
        </div>
    </div>
</body>
</html>